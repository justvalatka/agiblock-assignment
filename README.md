## Using the program


1. Click **Choose file** in the center of the screen.
2. Browse for CSV file.
3. Wait while file is being uploaded.
4. After upload, you will see a message that file has been uploaded.
5. Refresh the page.
6. Uploaded information will be seen in the table.


### Notes

Not every attribute is shown in UI. Only important information for user, all of the rest data is saved in local database.