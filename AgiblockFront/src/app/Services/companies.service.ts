import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Company } from '../Models/Company';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService extends BaseService {

  protected webUrl =this.webApiUrlBase + "/company"

  constructor(http: HttpClient) { 
    super(http);
  }

  public getCompanies(): Observable<Company[]>{
    return this.get<any>(this.webUrl);
  }

  public postCompany(company : any): Observable<any>{
    return this.post<any>(this.webUrl,company);
  }

}
