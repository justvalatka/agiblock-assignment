export class Company {
        public Id: number;
        public ExternalId: string;
        public TradingName: string;
        public LegalName: string;
        public CompanyType: number;
        public Unused: boolean;
        public IsForwarder: boolean;
        public Phone : string;
        public Fax: string;
        public AddressId?: number;
        public MailAddressId?: number;
        public IsCustomClearance: boolean;
        public IsActive: boolean;
        public IsCarrier: boolean;
        public IsWarehouse: boolean;
        public ChamberOfCommerce?: string;
        public ChamberOfCommerceCi?: string;
        public CountryVAT?: string; 
        public IsExchangeBroker: boolean;

        constructor(counterPartId: string, Name: string, type: number, phone: string, fax : string){
                this.ExternalId= counterPartId
                this.TradingName=Name
                this.LegalName= Name
                this.CompanyType= type
                this.Phone = phone
                this.Fax = fax
                this.Unused= false
                if(type==3){this.IsForwarder=true} else{this.IsForwarder=false}
                this.IsActive= true
                if(type == 0){this.IsCarrier= true} else{this.IsCarrier= false}
                this.IsWarehouse=true
                if(type == 2){this.IsExchangeBroker= true} else{this.IsExchangeBroker= false}
        }
}