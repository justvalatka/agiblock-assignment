import { Company } from './../../Models/Company';
import { CompaniesService } from './../../Services/companies.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css']
})
export class CompaniesComponent implements OnInit {
  public isUploaded = false;
  public isUploading = false;
  public dataSource: Company[];
  public dataToPush: Company[]= [];
  public dataFromCsv: any[];
  displayedColumns: string[] = ['ExternalId', 'TradingName', 'LegalName', 'Phone', 'Fax'];
  text  : any ;
  JSONData : any;

  constructor(private companiesService: CompaniesService) { }

  ngOnInit() {
    this.companiesService.getCompanies().subscribe(items => {this.dataSource = items;});
  }

  async csv(csvText) {
    var lines = csvText.split("\r");
 
    var result = [];
 
    var headers = lines[0].split(",");
    for (var i = 1; i < lines.length-1; i++) {
 
        var obj = {};
        var currentline = lines[i].split(",");
 
        for (var j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentline[j];
        }
 
        result.push(obj);
 
    }
    this.dataFromCsv= result;
    this.mapToModel(this.dataFromCsv);
    for(var element of this.dataToPush)
    {
      await this.companiesService.postCompany(element).subscribe(x=> (this.isUploaded=true));
    }
 }

  convertFile(input) {
  this.isUploading = true;
  this.dataFromCsv = [];
  const reader = new FileReader();
  if(input.files[0].name.includes('.csv')){
  reader.readAsText(input.files[0]);
  reader.onload =async () => {
    let text = reader.result;
    this.text = text;
    
    await this.csv(text).then( x=> {
      this.isUploading = false;
    });
    
  };
}else{
  console.log("File is not csv.")
}
}

  mapToModel(data : any[]){
    
    
    data.forEach(element => {
      var CounterPartID;
      var Name;
      var IsBuyer;
      var IsSeller;
      var Phone;
      var Fax;
      var Type: number;
      var company :Company;
      if(element.CounterPartID)
      {
        CounterPartID = element.CounterPartID;
      }
      if(element.Name)
      {
        Name = element.Name;
      }
      if(element.IsBuyer){
        IsBuyer=element.IsBuyer
      }
      if(element.IsSeller){
        IsSeller=element.IsSeller
      }
      if(element.Phone)
      {
        Phone=element.Phone
      }
      if(element.Fax)
      {
        Fax= element.Fax
      }
      if(IsBuyer && IsSeller)
      { Type = 2}
      if(!IsBuyer && !IsSeller)
      { Type = 3}
      if(IsBuyer && !IsSeller)
      { Type = 1}
      if(!IsBuyer && IsSeller)
      { Type = 0}
      company= new Company(CounterPartID,Name,Type,Phone,Fax);

      this.dataToPush.push(company);

    });
  }
}