﻿using Agiblock.Base;
using Agiblock.Models;
using Agiblock.Services.Interfaces;
using System.Web.Mvc;

namespace Agiblock.Controllers
{
    [Route("api/company")]
    public class CompanyController : BaseController<Company>
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService) : base(companyService)
        {
            _companyService = companyService;
        }
    }
} 