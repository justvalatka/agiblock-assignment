﻿using Agiblock.Base.Interface;
using Agiblock.Models;

namespace Agiblock.Services.Interfaces
{
    public interface ICompanyService : IBaseService<Company>
    {
    }
}