﻿using Agiblock.Base;
using Agiblock.Models;
using Agiblock.Repository.Interface;
using Agiblock.Services.Interfaces;

namespace Agiblock.Services
{
    public class CompanyService : BaseService<Company>, ICompanyService
    {
        private readonly IRepository _companyRepository;

        public CompanyService(IRepository companyRepository) : base(companyRepository)
        {
            _companyRepository = companyRepository;
        }
    }
}