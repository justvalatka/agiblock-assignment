﻿using System.ComponentModel.DataAnnotations;
using Agiblock.Base;

namespace Agiblock.Models
{
    public class Company : IBaseEntity
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string ExternalId { get; set; }
        public string TradingName { get; set; }
        public string LegalName { get; set; }
        public int CompanyType { get; set; }
        public bool Unused { get; set; }
        public bool IsForwarder { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
        [MaxLength(50)]
        public string Fax { get; set; }
        public int? AddressId { get; set; }
        public int? MailAddressId { get; set; }
        public bool IsCustomClearance { get; set; }
        public bool IsActive { get; set; }
        public bool IsCarrier { get; set; }
        public bool IsWarehouse { get; set; }
        public string ChamberOfCommerce { get; set; }
        public string ChamberOfCommerceCi { get; set; }
        public string CountryVAT { get; set; }
        public bool IsExchangeBroker { get; set; }
    }
}