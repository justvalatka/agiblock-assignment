﻿using Agiblock.Models;
using System.Data.Entity;

namespace Agiblock.Data
{
    public class ABContext : DbContext
    {
        public ABContext() : base()
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ABContext>());
        }

        public DbSet<Company> Companies { get; set; }
    }
}